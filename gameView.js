$(document).ready(Init);

function Init () {
    var textC = 0;
    
    // Cheats
//    $(".killBlock").click(function () {
//        users.shipPos();
//    });
	// end Cheats
	

	$("button").mouseenter(function () {
		$(this).css("background", "#C8C8F8");
		$(this).mouseleave(function () {
			if ($(this).hasClass("need")) {
				$(this).css("background", "#DDDDDD");
				return;
			}
			$(this).css("background", "whitesmoke");
		});
	});

	$(".start").bind("click", Game.start);
    $(".startGame").bind("click", Game.startGame);
    $(".restart").bind("click", Game.restartGame);
    $(".endGame").bind("click", Game.endGame);
    $(".autoFilling").bind("click", Game.autoFilling);
    $(".removeFill").bind("click", Game.removeFill);
    $(".direct>div").click(function (e) {
        $(".direct>div").removeClass("select");
        $(this).addClass("select");
        info.direction = +e.currentTarget.getAttribute("direction");
    });
    $(".filling>.ship, .direct>div").click(function (e) {
        if (e.currentTarget.hasAttribute("type")){
			if ($(this).hasClass("lock")) return;
			
            $(".filling>.ship").removeClass("selectedShip");
            info.shipType = +e.currentTarget.getAttribute("type");
			if ($(this).hasClass("one")) {
				$(".direct>div").addClass("lock");
				$(this).addClass("selectedShip")
				return;
			}
			$(".direct>div").removeClass("lock");
        }else{
            $(".direct>div").removeClass("selectedShip");
            info.direction = +e.currentTarget.getAttribute("direction");
        }
        $(this).addClass("selectedShip")
    });
    
	
	
    $(".infinity").click(function () {
        if (Game.intervalEnabled){
            clearInterval(Game.intervar);
            Game.intervalEnabled = false;
            textC = 0;
            $(".counter").html("");
            return;
        } 
        Game.intervalEnabled = true;
        Game.intervar = setInterval(function () {
            Game.restartGame();
            users.shipPos();
            textC++;
            $(".counter").html(textC * 2);
        }, $(".secretBlock>input:text").val());
        
    });
    $(".closeSecret").click(function () {
        $(".secretBlock").css("display", "none");
    });
    $(".missBlock>div").click(function () {
        Game.countSecret++;
        if (Game.countSecret === 5){
            $(".secretBlock").css("display", "block");
            Game.countSecret = 0;
        }
    });
}   

var Game = {
    countSecret: 0,
    intervalEnabled: false,
    intervalEnabled2 : 0,
    intervar: 0,
    saveMenu: 0,
                                                            
                                                    
    startEffect: function () {
        setTimeout(function () {
            $(".inspector").removeClass("shadow");
            $(".inspector").css("opacity", "0");
            $(".startGame").css("display", "none");
            $(".endGame").css("display", "block");
            setTimeout(function () {
                $(".inspector").css("display", "none");
                $(".table1, .table2, .leftSidebar, .rightSidebar").css("display", "block");
                $(".downBar").css("display", "flex");
                this.saveMenu = $(".inspector").detach();
            }, 500);
            setTimeout(function () {
                $("body").prepend(this.saveMenu);
                $(".restart").css({"display": "block", "margin-right": "40px"});
                $(".gameBox").css("margin-top", "3px");
                $(".table1, .table2, .downBar, .leftSidebar, .rightSidebar, .filling").css("opacity", "1");
                $(".filling").css("left", "0");
                
                $(".inspector").css({
                    position: "static",
                    display: "flex",
                    transition: "500ms",
                    margin: "0 auto",
                    flexFlow: "row nowrap"
                });
                setTimeout(function () {
                    $(".inspector").css({
                        opacity: 1,
                        marginBottom: "20px"
                    });
                    $(".table1, .table2, .downBar, .leftSidebar, .rightSidebar").addClass("shadow");
                }, 500);
                $(".rightSidebar, .leftSidebar").css("display", "flex");
                
                
            }, 800);
        }, 500);
    },
    endEffect: function () {
        Game.clearTable();
        $(".humanWon, .botWon").css("opacity", "0");
        $("td").removeAttr("style");
        $("td").removeAttr("class");
        $(".table1, .table2, .downBar, .leftSidebar, .rightSidebar").removeClass("shadow");
        $("#eGameBox tr>td").css("cursor", "default");
        setTimeout(function () {
            $(".table1, .table2, .downBar, .inspector, .leftSidebar, .rightSidebar, .filling").css("opacity", "0");
			$(".filling").css("left", "-270px");
            setTimeout(function () {
                $(".startGame").css("display", "block");
                $(".endGame").css("display", "none");
            }, 500);
            setTimeout(function () {
                $(".rightSidebar, .leftSidebar").css("display", "none");
                $(".gameBox").append(this.saveMenu);
                $(".restart").css({"display": "none", "margin-right": "0"});
                $(".gameBox").css("margin-top", "100px");
                $(".inspector").css({
                    position: "static",
                    display: "flex",
                    transition: "500ms",
                    flexFlow: "column nowrap"
                });
                $(".inspector").css("opacity","1");
                setTimeout(function () {
                    $(".inspector").addClass("shadow");
                }, 500);
            }, 800);
        }, 500);
        $("td").addClass("defaultTd");
    },
    restartEffect: function () {
        $(".humanWon, .botWon").css("opacity", "0");
        $("td").removeAttr("style");
		$("td").removeAttr("class");
        $(".rightSidebar .hits>span").html(0);
        $(".rightSidebar .misses>span").html(0);
        $(".rightSidebar .kills>span").html(0);
        $(".leftSidebar .hits>span").html(0);
        $(".leftSidebar .misses>span").html(0);
        $(".leftSidebar .kills>span").html(0);
		$(".filling").animate({left:"0"}, 400);
		$(".four").trigger("click");
		$(".ver").trigger("click");
    },
    
    
    startGame: function () {
        Game.startEffect();
		Game.bindGenerate2("on");
    },
    restartGame: function () {
		if ($(".restart").hasClass("disabled")) return;
        Game.restartEffect();
        Game.clearTable();
   		users.endGame();
		bot.endGame();
		Game.bindGenerate("off");
		Game.bindGenerate2("on");
		$("#mGameBox tr>td").css("cursor", "cell");
		$("#eGameBox tr>td").css("cursor", "default");
		$(this).addClass("disabled");
    },
    endGame: function () {
        Game.endEffect();
        $("#eGameBox tr>td").unbind("click");
        $("#eGameBox tr>td").unbind("mouseenter");
        users.endGame();
        bot.endGame(); 
    },
	start: function () {
		if ($(".start").hasClass("disabled")) return; 
		$(".filling").animate({
			left : "-270px"
		}, 400, function () {
			info.removeData();
			Game.bindGenerate("on");
        	bot.access = true;
        	users.access = true;
        	users.restartGame();
			$("#mGameBox tr>td").removeClass("miss");
			Game.bindGenerate2("off");
			Game.bindGenerate("on");
			$("#mGameBox tr>td").css("cursor", "default");
			$("#eGameBox tr>td").css("cursor", "crosshair");
			$(".restart").removeClass("disabled");
			$(".removeFill").addClass("disabled");
			$(".start").addClass("disabled");
		});	
	},
    autoFilling: function () {
        $("td").removeAttr("style");
        $("td").removeAttr("class");
        bot.restartGame();
        bot.access = true;
        bot.shipPos();
		$(".start").removeClass("disabled");
		$(".removeFill").removeClass("disabled");
		bot.shipCol();
    },
    removeFill: function () {
		if ($(".removeFill").hasClass("disabled")) return; 
        $("td").removeAttr("class");
        $("td").removeAttr("style");
        bot.endGame();
        info.removeData();
		$(".start").addClass("disabled");
		$(this).addClass("disabled");
    },
    
    clearTable: function () {
        $(".rightSidebar .hits>span").html(0);
        $(".rightSidebar .misses>span").html(0);
        $(".rightSidebar .kills>span").html(0);
        $(".leftSidebar .hits>span").html(0);
        $(".leftSidebar .misses>span").html(0);
        $(".leftSidebar .kills>span").html(0);
        clearInterval(Game.intervalEnabled2);
    },
    bindGenerate: function (onOrOff) {
        if (onOrOff == "on"){
            $("#eGameBox tr>td").bind("click", function () {
                $(this).removeClass("selected");
                if (users.checkDouble(this.id)){
                    if (users.hited) return;
                    bot.smartFire();
                    if (bot.hited) {
                        users.access = false;
                        Game.bindGenerate("off");
                        Game.intervalEnabled2 = setInterval(function () {
                            if (!bot.hited) {
                                users.access = true;
                                Game.bindGenerate("on");
                                clearInterval(Game.intervalEnabled2);
                                return false;
                            }
                            console.log("Права хода " +bot.access);
                            if (!bot.access) return; 
                            bot.smartFire();
                        }, 300);    
                    }
                }
            });
            $("#eGameBox tr>td").bind("mouseenter", function () {
                if ($(this).hasClass("miss") 
                || $(this).hasClass("hit") 
                || $(this).hasClass("kill")) return;

                $(this).addClass("selected");
                $(this).mouseleave(function () {
                    $(this).removeClass("selected");
                });
            });
        }
        else {
            $("#eGameBox tr>td").unbind("click");
            $("#eGameBox tr>td").unbind("mouseenter");
        }
         
    },
	bindGenerate2: function (onOrOff) {
		if (onOrOff == "on"){
			$("#mGameBox tr>td").mousemove(function () {
			info.nowPos = [];
			info.nowCol = [];
			$("td[class!='j']").removeClass("selecting");
			$("td[class!='c']").removeClass("selecting");
			var row, col;
			row = +this.id.charAt(0);
			col = +this.id.charAt(1);
			if (!info.direction) {
				if (row > -1 && row < (10 - (info.shipType - 1))) {
					if (   info.filledShips.includes(row+""+col) 
						|| info.filledShips.includes((row+(info.shipType-1))+""+col) 
						|| info.filledCollisions.includes(row+""+col) 
						|| info.filledCollisions.includes((row+(info.shipType-1))+""+col)
					   ) return;

					for (var i = 0; i < info.shipType; i++) {
						info.nowPos.push((row+i)+""+col);
					}

					for (var ch = 0; ch < info.nowPos.length; ch++) {
						if (info.filledShips.includes(info.nowPos[ch]) || info.filledCollisions.includes(info.nowPos[ch])) {
							return;
						} 
					}

					for (var i = 0; i < info.shipType; i++) {
						$("tr>#" + (row + i) + "" + col).addClass("selecting");
					}

					info.nowCol.push((row-1)+""+col);
					info.nowCol.push((row+info.shipType>=10?-1:row+info.shipType)+""+col);
					info.nowCol.push((row-1)+""+(col-1));
					info.nowCol.push((row-1)+""+(col+1>=10?-1:col+1));
					info.nowCol.push((row+info.shipType>=10?-1:row+info.shipType)+""+(col-1));
					info.nowCol.push((row+info.shipType>=10?-1:row+info.shipType)+""+(col+1>=10?-1:col+1));
					for (var c = 0; c < info.shipType; c++) {
						info.nowCol.push((row+c>=10?-1:row+c)+""+(col+1>=10?-1:col+1));
						info.nowCol.push((row+c>=10?-1:row+c)+""+(col-1));
					}
				}
			}else{
				if (col > -1 && col < (10 - (info.shipType - 1))){
					if (   info.filledShips.includes(row+""+col) 
						|| info.filledShips.includes(row+""+(col+(info.shipType - 1)))
						|| info.filledCollisions.includes(row+""+col)
						|| info.filledCollisions.includes(row+""+(col+(info.shipType - 1)))
					   ) return;

					for (var i = 0; i < info.shipType; i++) {
						info.nowPos.push(row+""+(col+i));
					}
					for (var ch = 0; ch < info.nowPos.length; ch++) {
						if (info.filledShips.includes(info.nowPos[ch]) || info.filledCollisions.includes(info.nowPos[ch])) {
							return;
						} 
					}

					for (var i = 0; i < info.shipType; i++) {
						$("tr>#" + row + "" + (col + i)).addClass("selecting");
					}


					info.nowCol.push(row+""+(col-1));
					info.nowCol.push(row+""+(col+info.shipType>=10?-1:col+info.shipType));
					info.nowCol.push((row-1)+""+(col-1));
					info.nowCol.push((row+1>=10?-1:row+1)+""+(col-1));
					info.nowCol.push((row-1)+""+(col+info.shipType>=10?-1:col+info.shipType));
					info.nowCol.push((row+1>=10?-1:row+1)+""+(col+info.shipType>=10?-1:col+info.shipType));
					for (var c = 0; c < info.shipType; c++) {
						info.nowCol.push((row+1>=10?-1:row+1)+""+(col+c>=10?-1:col+c));
						info.nowCol.push((row-1)+""+(col+c>=10?-1:col+c));
					}
				}

			}
			for (var i = 0; i < info.nowCol.length; i++) {
						if (  info.nowCol[i].search(/\-\d\-\d/)>-1
							||info.nowCol[i].search(/\-\d\d/)>-1
							||info.nowCol[i].search(/\d\-\d/)>-1){
							info.nowCol[i] = info.nowCol[i].replace(/\-\d\-\d/, "");
							info.nowCol[i] = info.nowCol[i].replace(/\-\d\d/, "");
							info.nowCol[i] = info.nowCol[i].replace(/\d\-\d/, "");
						}
				}
				
			var readyCollisions = [];
			for (var i=0; i<info.nowCol.length; i++){
				if (!info.nowCol[i]) continue;
				readyCollisions.push(info.nowCol[i]);
			}
				info.nowCol = readyCollisions;
			$(this).mouseleave(function () {
				$("td[class!='j']").removeClass("selecting");
				$("td[class!='miss']").removeClass("selecting");
			});
    });
			$("#mGameBox tr>td").click(function () {
				if (info.fillCount == 10 || info.quantityShips[info.nowPos.length].full) {
					return;
				}
				
				for (var i = 0; i < info.nowPos.length; i++){
					   $("td#" + info.nowPos[i].charAt(0) + "" +   info.nowPos[i].charAt(1)).addClass("j");
				}
				
				info.filledShips += info.nowPos.join()+',';
				info.filledCollisions += info.nowCol.join()+',';

				for (var i = 0; i < info.filledCollisions.length; i++) {
					info.filledCollisions = info.filledCollisions.replace(/\,\,/, ",");
				}
				for (var j = 0; j < info.nowCol.length; j++){
					$("td#"+info.nowCol[j].charAt(0) + "" +
					info.nowCol[j].charAt(1)).addClass("miss");
				}
        
        
				if (info.nowCol.length < 1) return;
				
				bot.ships[info.fillCount].locations = info.nowPos;
				bot.ships[info.fillCount].collision = info.nowCol;
				bot.ships[info.fillCount].hits = [];
				for (var i = 0; i < info.nowPos.length; i++) {
					bot.ships[info.fillCount].hits.push("");
				}
				bot.ships[info.fillCount].length = info.nowPos.length;
				
				info.fillCount++;
				info.quantityShips[info.nowPos.length].quantity++;
				
				if (info.quantityShips[info.nowPos.length].max == info.quantityShips[info.nowPos.length].quantity){
					Game.lockSelectShip(info.nowPos.length);
					info.quantityShips[info.nowPos.length].full = true;
				}
				
				
				if (info.fillCount) {				
					$(".removeFill").removeClass("disabled");
				}
				if (info.fillCount == 10){
					$(".direct>div").addClass("lock");
					$(".start").removeClass("disabled");
					return;
				}
		
    });
		}else{
			$("#mGameBox tr>td").unbind("mousemove");
			$("#mGameBox tr>td").unbind("click");
		}	
	},
    win: function (type) {
        if (type === "u") $(".humanWon").css("opacity", "1");
        else $(".botWon").css("opacity", "1");
        bot.access = false;
        users.access = false;
		Game.bindGenerate("off");
		$("#eGameBox tr>td").css("cursor", "default");
    },
	lockSelectShip: function (num) {
		$("#s"+num).addClass("lock");
	}
}

var info = {
    fillCount: 0,
    direction: 0,
	quantityShips: ["just",
					{max: 4, quantity: 0, full: false},
					{max: 3, quantity: 0, full: false},
					{max: 2, quantity: 0, full: false},
					{max: 1, quantity: 0, full: false}], 
    nowPos: [],
    nowCol: [],
    filledShips: '',
    filledCollisions: '',
    shipType: 4,
    removeData: function () {
        this.fillCount = this.direction = 0;
        this.nowPos = this.nowCol = [];
        this.filledShips = this.filledCollisions = '';
		for (var i = 0; i < this.quantityShips.length; i++){
			if (!i) continue;
			this.quantityShips[i].quantity = 0;
			this.quantityShips[i].full = false;
		}
		$("[type], [direction]").removeClass("lock");
    }
}